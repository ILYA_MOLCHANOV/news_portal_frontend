

// Экспортируем функцию card для использования в других модулях
export const card = (article) => {
    return `
        <div class="card">
                <div class="card__img">
                    <img src="${article.image}" alt="img">
                </div>
            <div class="card__body">
                <p class="card__title">${article.title}</p>
                <p class="card__description">${article.content}</p>
                <p class="card__time">${article.createdAt.split('T')[0].split('-').reverse().join('.')}</p>
            </div>
        </div>
    `;
}