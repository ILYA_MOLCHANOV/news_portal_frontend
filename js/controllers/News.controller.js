
/*Этот код представляет класс NewsController,
    который управляет получением, созданием и отображением новостей на веб-странице.
    Класс также инициализирует интервал для обновления новостей каждые 10 секунд.*/
// Импортируем необходимые модули
// Модуль для создания HTML-разметки карточки новости
import { card } from "../card/card.js";
// Модуль для работы с новостями на сервере
import { NewsService } from "../services/News.service.js";
// Создаем класс NewsController для управления новостями
class NewsController {
    // Приватное поле для хранения данных о новостях
    #data = [];
    // Создаем экземпляр сервиса для работы с новостями
    #newsService = new NewsService();
    // Асинхронно получаем все новости
    async getAllNews() {
        const response = await this.#newsService.getAllNews();
        this.#data.push(...response);
    }
    // Асинхронно создаем новую статью
    async createArticle(body) {
        await this.#newsService.createArticle(body);
    }
    // Отображаем новости на странице
    render() {
        const grid = document.querySelector('.news__grid');
        grid.innerHTML = '';
        this.#data.forEach(article => {
            grid.innerHTML += card(article);
        })
    }
    // Инициализация контроллера
    async init() {
        await this.getAllNews(); // Получаем все новости
        await this.render(); // Отображаем их на странице
        // Устанавливаем интервал обновления новостей каждые 10 секунд
        setInterval(async () => {
       	   this.#data = []; // Очищаем данные
           await this.getAllNews(); // Получаем новые данные
           await this.render();  // Отображаем их на странице
        }, 10000) // Интервал 10 секунд (10000 миллисекунд)
    }
}
// Создаем экземпляр класса NewsController
const newsController = new NewsController();
// Экспортируем экземпляр контроллера для использования в других модулях
export default newsController;