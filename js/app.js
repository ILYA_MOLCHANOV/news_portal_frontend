// Импортируем модуль News.controller.js из директории './controllers'
import newsController from './controllers/News.controller.js';
// Инициализируем контроллер для работы с новостями
newsController.init();