
/*Этот код отслеживает отправку формы, проверяет заполнение полей, выводит сообщения об ошибке
и успешном создании, и удаляет сообщения об ошибке при фокусировке на полях.*/


// Импортируем модуль News.controller.js из директории './controllers'
import newsController from './controllers/News.controller.js';

// Получаем ссылки на HTML-элементы формы и другие элементы
const form  = document.querySelector('.form');
const button = document.querySelector('.button');
const title = document.querySelector('.name');
const content = document.querySelector('.content');
const error1 = document.querySelector('.error_1');
const error2 = document.querySelector('.error_2');
const success = document.querySelector('.success');

// Обработчик события "submit" формы
form.addEventListener('submit', (e) => {
    e.preventDefault(); // Предотвращаем отправку формы по умолчанию

    // Проверяем, если поле заголовка пустое
    if (title.value === '') {

        // Добавляем класс 'error' для отображения сообщения об ошибке
        error1.classList.add('error');
        setTimeout(() => {

            if (error1.classList.contains('error')) {
                // Удаляем класс 'error' через 3 секунды
                error1.classList.remove('error')
            }
        }, 3000)
    }

    // Проверяем, если поле описания пустое
    if (content.value === '') {
        // Добавляем класс 'error' для отображения сообщения об ошибке
        error2.classList.add('error');
        setTimeout(() => {
            if (error2.classList.contains('error')) {
                error2.classList.remove('error')  // Удаляем класс 'error' через 3 секунды
            }
        }, 3000)
    }

    // Если оба поля не пустые, создаем объект 'body' и вызываем функцию 'createArticle' контроллера
    if (title.value !== '' && content.value !== '') {
        const body = {
            title: title.value,
            content: content.value
        }
        newsController.createArticle(body);
        title.value = '';
        content.value = '';
        success.textContent = 'Пост успешно создан'; // Выводим сообщение об успешном создании
        setTimeout(() => {
            success.textContent = ''; // Удаляем сообщение об успешном создании через 2 секунды
        }, 2000)
    }
})

// Обработчики события "focus" для полей заголовка и описания, удаляют класс 'error' при фокусировке
title.addEventListener('focus', () => {
    if (error1.classList.contains('error')) {
        error1.classList.remove('error')
    }
})
content.addEventListener('focus', () => {
    if (error2.classList.contains('error')) {
        error2.classList.remove('error')
    }
})